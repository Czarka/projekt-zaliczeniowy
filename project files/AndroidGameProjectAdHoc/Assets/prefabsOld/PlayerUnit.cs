﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerUnit : NetworkBehaviour
{
    public float speed = 5.0f;
    public Joystick joystick;

    void Start()
    {
        Debug.Log("start");
        joystick = GameObject.Find("BackGroundJoystick").GetComponent<Joystick>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("work");
        if (hasAuthority == false)
            return;

        transform.Translate(getInput()*speed);
    }

    private Vector3 getInput()
    {
        Vector3 dir = Vector3.zero;

        dir = joystick.getVector();

        if (dir.magnitude > 1)
        {
            dir.Normalize();
        }
        Debug.Log(dir);
        return dir;
    }
}
