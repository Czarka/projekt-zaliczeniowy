﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

//class for dealing with connecting playes using brodcast
public class DiscoverGame : MonoBehaviour
{
    const int kMaxBroadcastMsgSize = 1024;

    // config data
    [SerializeField]
    public int m_BroadcastPort = 47777;

    [SerializeField]
    public int m_BroadcastKey = 1000;

    [SerializeField]
    public int m_BroadcastVersion = 1;

    [SerializeField]
    public int m_BroadcastSubVersion = 1;

    [SerializeField]
    public string m_BroadcastData = "Msg";

    [SerializeField]
    public bool m_ShowGUI = true;

    [SerializeField]
    public int m_OffsetX;

    [SerializeField]
    public int m_OffsetY;

    // runtime data
    public int hostId = -1;
    public bool running = false;

    public Text consoleText;

    byte[] msgOutBuffer = null;
    byte[] msgInBuffer = null;
    HostTopology defaultTopology;

    

    static byte[] StringToBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    static string BytesToString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

    public bool Initialize()
    {
        if (m_BroadcastData.Length >= kMaxBroadcastMsgSize)
        {
            Debug.LogError("NetworkDiscovery Initialize - data too large. max is " + kMaxBroadcastMsgSize);
            return false;
        }

        if (!NetworkTransport.IsStarted)
        {
            NetworkTransport.Init();
        }

        if (NetworkManager.singleton != null)
        {
            m_BroadcastData = "NetworkManager:" + NetworkManager.singleton.networkAddress + ":" + NetworkManager.singleton.networkPort;
        }

        msgOutBuffer = StringToBytes(m_BroadcastData);
        msgInBuffer = new byte[kMaxBroadcastMsgSize];

        ConnectionConfig cc = new ConnectionConfig();
        cc.AddChannel(QosType.Unreliable);
        defaultTopology = new HostTopology(cc, 1);

        StartBrodcast();

        return true;
    }


    public bool StartBrodcast()
    {
        if (hostId != -1 || running)
        {
            Debug.LogWarning("NetworkDiscovery StartAsServer already started");
            return false;
        }

        hostId = NetworkTransport.AddHost(defaultTopology, 0);
        if (hostId == -1)
        {
            Debug.LogError("NetworkDiscovery StartAsServer - addHost failed");
            return false;
        }

        byte err;
        if (!NetworkTransport.StartBroadcastDiscovery(hostId, m_BroadcastPort, m_BroadcastKey, m_BroadcastVersion, m_BroadcastSubVersion, msgOutBuffer, msgOutBuffer.Length, 1000, out err))
        {
            Debug.LogError("NetworkDiscovery StartBroadcast failed err: " + err);
            return false;
        }

        running = true;
        Debug.Log("StartAsServer Discovery broadcasting");
       
        DontDestroyOnLoad(gameObject);
        return true;
    }


    public void StopBroadcast()
    {
        if (hostId == -1)
        {
            Debug.LogError("NetworkDiscovery StopBroadcast not initialized");
            return;
        }

        if (!running)
        {
            Debug.LogWarning("NetworkDiscovery StopBroadcast not started");
            return;
        }
        NetworkTransport.StopBroadcastDiscovery();
       

        NetworkTransport.RemoveHost(hostId);
        hostId = -1;
        running = false;

        msgInBuffer = null;
        Debug.Log("Stopped Discovery broadcasting");
    }


    void Start()
    {
        Initialize();
    }

}
