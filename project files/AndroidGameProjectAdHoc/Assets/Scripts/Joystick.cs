﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    private Image backgroundImage;
    private Image joystickImage;
    private Vector3 inputVector;

    private bool JoysticMove=false;


    void Start()
    {
        backgroundImage = GetComponent<Image>();

        joystickImage=transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        Vector2 position;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImage.rectTransform, eventData.position, eventData.pressEventCamera, out position)) 
        {
            
            position.x =( position.x / backgroundImage.rectTransform.sizeDelta.x*3);
            position.y = (position.y / backgroundImage.rectTransform.sizeDelta.y*3);

            inputVector = new Vector3(position.x*2 +1, position.y*2 -1,0);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;
           
            joystickImage.rectTransform.anchoredPosition = new Vector3(inputVector.x * (backgroundImage.rectTransform.sizeDelta.x / 4), inputVector.y * (backgroundImage.rectTransform.sizeDelta.y / 4));
            JoysticMove = true;
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        inputVector = Vector3.zero;
        joystickImage.rectTransform.anchoredPosition = Vector3.zero;
        JoysticMove = false;
    }

    public float Horizontal()
    {
        if (inputVector.x != 0)
            return inputVector.x;
        else
            return Input.GetAxis("Horizontal");
    }

    public float Vertical()
    {
        if (inputVector.z != 0)
            return inputVector.z;
        else
            return Input.GetAxis("Vertical");
    }

    public bool isJoysticMoved()
    {
        return JoysticMove;
    }

    public Vector3 getVector()
    {
       
        return inputVector;
    }
}
