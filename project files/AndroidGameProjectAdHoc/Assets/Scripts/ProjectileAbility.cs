﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/ProjectileAbility")]
public class ProjectileAbility : Ability //a "interface" for Abiliteis that are creating projectiles
{
    BulletManager bulletManager;

    public int amount_of_bullets;
    public float time_interval;
    public int angle_between_bullets;

    // public Vector2 starPosition;

    public override void Initialize(GameObject obj)
    {
        Debug.Log(obj.name);
        bulletManager = obj.GetComponent<BulletManager>();
        bulletManager.Init(amount_of_bullets, time_interval, angle_between_bullets);
        //launcher.bullettemplate.direction = direction;
        //launcher.bullettemplate.startPosition = starPosition;

        //launcher.bullettemplate.Init();

    }

    public override void TriggerAbility()
    {
        bulletManager.Init(amount_of_bullets, time_interval, angle_between_bullets);
        bulletManager.Fire();

    }

}
