﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : PhysicsManager
{

    private float speed = 200.0f;
    private float jumpStrenght = 500.0f;

    private int HealthBarPositionXPlayer=316;
    private int HealthBarPositionXOpponent=-314;
    private int HealthBarPositionY=183;

    public Vector2 position;
    public Joystick joystick;


    //buttons have countdown script attached to them remembre to set countdown time 
    Button JumpButton;

    Button SkillButton1;

    Button SkillButton2;

    Character playerCharacter;

    public HealthBar Hp;

    [SerializeField]
    GameObject HealthBarObjectPrefab;

    //[SerializeField]
    GameObject HealthBarObject;

    public BulletManager bulletManager;

    private bool isFacingRight = true; //false means character is facing left

    public Ability[] playerAbilities;

    private Animator animator;

    public delegate void OnPlayerSpawnedCallbackDelegate();
    static public event OnPlayerSpawnedCallbackDelegate OnPOSpawnedListeners;

    public delegate void OnDeathCallbackDelegate();
    static public event OnDeathCallbackDelegate OnDeathListeners;



    void Start()
    {
        transform.Translate(position);

        int selectedchrId=0;
        if (gameObject.tag == "enemyplayer") //object that is represnting don't need it's skills intialized because everything will be sended
        {

            selectedchrId = GameObject.Find("_app").GetComponent<GameManger>().selectedOpponentCharacter;
            playerCharacter = GameObject.Find("_app").GetComponent<GameManger>().characters[selectedchrId - 1];
          
        }
        else if (gameObject.tag == "localplayer")
        {
            selectedchrId = GameObject.Find("_app").GetComponent<GameManger>().selectedCharacter;
            playerCharacter = GameObject.Find("_app").GetComponent<GameManger>().characters[selectedchrId - 1];
            playerAbilities = playerCharacter.characterAbilities;
            playerAbilities[0].Initialize(gameObject);
            playerAbilities[1].Initialize(gameObject);
        }
     
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        GameObject globalCanvas = GameObject.Find("GameManuals"); //canvas for healthbars

        //Init HealthBars
        HealthBarObject = Instantiate(HealthBarObjectPrefab, new Vector2(90, 415), Quaternion.identity, globalCanvas.transform);
        HealthBarObject.transform.localPosition = new Vector2(HealthBarPositionXPlayer, HealthBarPositionY);
        Hp = HealthBarObject.GetComponentInChildren<HealthBar>();
        Hp.init(playerCharacter.startingHp, playerCharacter.startingHp);
        HealthBarObject.GetComponentInChildren<HealthBar>().init(playerCharacter.startingHp, playerCharacter.startingHp);

       

        if (gameObject.tag == "enemyplayer")
        {
            HealthBarObject.transform.localPosition = new Vector2(HealthBarPositionXOpponent, HealthBarPositionY);

        }
        else if (gameObject.tag == "localplayer")
        {
            JumpButton = GameObject.Find("JumpButton").GetComponent<Button>();
            JumpButton.onClick.AddListener(jump);
            SkillButton1 = GameObject.Find("Skill1Button").GetComponent<Button>();
            SkillButton1.GetComponent<CountDownScript>().countdownTime = playerAbilities[0].aBaseCoolDown;
            SkillButton1.onClick.AddListener(TriggerSkill1);

            SkillButton2 = GameObject.Find("Skill2Button").GetComponent<Button>();
            SkillButton2.GetComponent<CountDownScript>().countdownTime = playerAbilities[1].aBaseCoolDown;
            SkillButton2.onClick.AddListener(TriggerSkill2);

            joystick = GameObject.Find("BackGroundJoystick").GetComponent<Joystick>();

            OnPOSpawnedListeners?.Invoke();
        }

    }




    public void AnimateMovment (Vector2 direction) // taht function is giving me information if player object is faceing right or left and is also giving this information to animator
    {
       if(direction.x >0)
        {
            isFacingRight = false;
        }
        else if(direction.x<0)
        {
            isFacingRight = true;
        }
        animator.SetFloat("side", direction.x);
    }


    private void isPlayerAlive() //returns if player spełnia warunki czy jest martwy
    {
        if (Hp.currentValue < 0 || transform.position.y < -100)
        {
            OnDeathListeners?.Invoke();
            Destroy(HealthBarObject);
            Destroy(gameObject);

        }

    }


    void OnBecameInvisible() //player is out of map
    {
        OnDeathListeners?.Invoke();
        Destroy(HealthBarObject);
        Destroy(gameObject);
    }

    public void jump()
    {
        velocity.y = jumpStrenght;     
    }

    public void TriggerSkill1() //aktywacja pierwszej umiejętności (powiązana z pierwszym guzikiem)
    {

        Vector2 bulletDirection = joystick.getVector();

        if(bulletDirection.x == 0 && isFacingRight ==true) //the direction of bullet is chosen with input from joystick, if ther is no imput bullets fires into direction player is facing
        {
            bulletDirection.x = -1;
        }
        else if (bulletDirection.x == 0 && isFacingRight == false)
        {
            bulletDirection.x = 1;
        }
        bulletManager.bulletDirection = bulletDirection;
       
       
       // Debug.Log(bulletManager.gameObject.name + "fires bullet");
        playerAbilities[0].TriggerAbility();

    }

    public void TriggerSkill2() //aktywacja drugiej umiejetności
    {

        Vector2 bulletDirection = joystick.getVector();
        if (bulletDirection.x == 0 && isFacingRight == true)
        {
            bulletDirection.x = -1;
        }
        else if (bulletDirection.x == 0 && isFacingRight == false)
        {
            bulletDirection.x = 1;
        }
        bulletManager.bulletDirection = bulletDirection;


       // Debug.Log(bulletManager.gameObject.name + "fires bullet");
        playerAbilities[1].TriggerAbility();

    }


    public void move (Vector2 position)
    {

        Debug.Log( "move "+ position);
        if (position.x > transform.position.x)
        {
            isFacingRight = true;
            animator.SetFloat("side", 1);
        }
        else if (position.x < transform.position.x)
        {
            isFacingRight = false;
            animator.SetFloat("side", -1);
        }

        transform.position = position;
        

    }

    public void move()
    {
       
            if (joystick != null && joystick.isJoysticMoved())
            {
                Vector3 dir = Vector3.zero;
                dir = joystick.getVector();
                AnimateMovment(dir);

                if (dir.y > 0)
                {
                    dir.y = 0; //jumping for going up, we don't want a plyer going up with joystick
                }

                if (dir.magnitude > 1)
                {
                    dir.Normalize();
                }

                targetVelocity = (Vector2)dir * speed;

            }
        isPlayerAlive();


    }

    protected override void ComputeVelocity()
    {
        position = Vector2.zero;
        //base.ComputeVelocity();
        move();
    }

}
