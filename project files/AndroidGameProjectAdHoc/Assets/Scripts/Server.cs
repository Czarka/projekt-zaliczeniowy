﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Server : MonoBehaviour //should be called listner beacause this function listen to opponent and making their actions for player
{
    private byte reliableChannel;

    private const int MAX_USER = 3;
    private  int PORT = 77777;
    private const int BYTE_SIZE = 1024;
    private int hostId;

    private byte error;


    private bool isStarted;

    private bool opponentConnected = false;

    GameObject playerObject;

    PlayerManager playerManager;

    List<GameObject> OtherPlayersBullet;


    public delegate void OnDisconnectCallbackDelegate();
    static public event OnDisconnectCallbackDelegate DisconnectListeners;

    public delegate void OnOpponentLeftMatchCallbackDelegate();
    static public event OnDisconnectCallbackDelegate OpponentLeftListeners;

    void Start()
    {

        DontDestroyOnLoad(this);
        Init();

    }



    void Init()
    {
        
        NetworkTransport.Init();

        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);


        HostTopology topo = new HostTopology(cc, MAX_USER);

        if (!GameObject.Find("_app").GetComponent<GameManger>().ishosting)
        {
            Debug.Log("not hosting");

            PORT++;
        }

        hostId = NetworkTransport.AddHost(topo, PORT, null);

        isStarted = true;
        //Debug.Log("Init server on "+PORT+" on address:"+ LocalIPAddress());

        OtherPlayersBullet = new List<GameObject>();
    }

    public void Shutdown()
    {
        isStarted = false;
        NetworkTransport.Shutdown();
    }


    private string LocalIPAddress()//return local adress
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }

    void sendToClient(int connectionId, GameMessage msg) //formating bufer and sending mesage not realy used right now
    {
        byte[] buffer = new byte[BYTE_SIZE];



        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream(buffer);
        formatter.Serialize(memoryStream, msg);

        NetworkTransport.Send(hostId, connectionId, reliableChannel, buffer, BYTE_SIZE, out error);
       // Debug.Log("sending data to server");
    }


    public void UpdateMessagePump() //reciving and dealing with msg from opponent
    {
        if (!isStarted)
            return;
        int recHostId;
        int connectionId;
        int channelId;

        byte[] recBuffer = new byte[BYTE_SIZE];
        int datasize;

        NetworkEventType type= NetworkTransport.Receive(out hostId, out connectionId, out channelId, recBuffer, recBuffer.Length, out datasize, out error);

        switch (type)
        {
            case NetworkEventType.DataEvent:
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream(recBuffer);
                GameMessage msg = (GameMessage)formatter.Deserialize(memoryStream);
                // formatter.Serialize(memoryStream, msg);
                OnData(connectionId, channelId, hostId, msg);
                //Debug.Log(recBuffer[0]);
                break;

            case NetworkEventType.ConnectEvent:
                Debug.Log("Connected Listener");

                opponentConnected = true;
                if (gameObject.GetComponent<Client>() != null)
                {
                    gameObject.GetComponent<Client>().SendReady();
                }
               

                break;

            case NetworkEventType.DisconnectEvent:
                Debug.Log("Disconnected");

                DisconnectListeners?.Invoke();
                
                opponentConnected = false;
                StartCoroutine(DisconnectionWaiting());
                break;

            case NetworkEventType.BroadcastEvent:
                break;

            case NetworkEventType.Nothing:
                break;

            default:
                break;
        }
    }


    IEnumerator DisconnectionWaiting() //wait 8 second before invoke a event confirming that player disconnected
    {
        yield return new WaitForSecondsRealtime(8);
        if (opponentConnected == false)
        {
            Debug.Log("Opponent disconnected for good");

            OpponentLeftListeners?.Invoke();

        }
    }

    void Update()
    {
        UpdateMessagePump();
    }




    private void OnData(int cnnId, int channelId, int hostId, GameMessage msg) //for dealing with data sended from opponent
    {
        Debug.Log("recived " + msg.operationCode);
        string debug = ("recived " + msg.operationCode);
        //ServerMessages.text = debug;
        switch (msg.operationCode) //making actions acording to msg opering code described in GameMessage.cs
        {
            case GameOP.None:
              //  Debug.Log("none case ");
                break;

            case GameOP.Position:
                //Debug.Log("pos case ");
                UpdatePosition((Game_Position)msg);
                break;

            case GameOP.Init:
                //Debug.Log("init case ");
                InitPlayer((Game_Init)msg);
                break;

            case GameOP.BulledSpawned:
                SpawnBullet((Game_SpawnBullet)msg);
                break;

            case GameOP.BulletInformation:
                HandleBullet((Game_Bullet)msg);
                break;

            case GameOP.DestroyBullet:
                DestroyBullet((Game_DestroyBullet)msg);
                break;

            case GameOP.Health:
                UpdateHealth((Game_PlayerLifeChanged)msg);
                break;

            case GameOP.SendingAddres:
                connectClientWithOpponnetListener((Game_SendAddress)msg);
                break;

            case GameOP.Ready:
                CreateandSendYourPO();
                break;


            case GameOP.Death:
                HandlePlayerDeath((Game_Death)msg);
                break;



        }
    }

    private void InitPlayer(Game_Init gimsg)
    {

        GameObject.Find("_app").GetComponent<GameManger>().selectedOpponentCharacter = gimsg.chosenchatacter; //opponent choice
        playerObject = Instantiate(GameObject.Find("_app").GetComponent<GameManger>().OpponentPrefabs[gimsg.chosenchatacter - 1], new Vector3(gimsg.x, gimsg.y, 0), Quaternion.identity);
        playerObject.tag = "enemyplayer";
        playerObject.name = "OpponentPlayerObject";
        playerManager = playerObject.GetComponent<PlayerManager>();
    }


    private void UpdatePosition(Game_Position gpmsg)
    {
        Debug.Log("pos" + gpmsg.x + "  " + gpmsg.y);
        if (playerObject != null)
            playerManager.move(new Vector2(gpmsg.x, gpmsg.y));

    }

    private void UpdateHealth(Game_PlayerLifeChanged ghmsg)
    {
        playerManager.Hp.SetHealth(ghmsg.currentHealthValue);
    }

    private void SpawnBullet(Game_SpawnBullet gsmsg)
    {
        GameObject go = Instantiate(playerObject.GetComponent<BulletManager>().bulletPrefab, new Vector3(gsmsg.x, gsmsg.y, 0), Quaternion.identity);
        go.GetComponent<Bullet>().InitOnServer(new Vector2(gsmsg.dirX, gsmsg.dirY), gsmsg.Id);
        OtherPlayersBullet.Add(go);
       
    }

    private void DestroyBullet(Game_DestroyBullet gbmsg)
    {

        GameObject bl = OtherPlayersBullet.Find(b => b.GetComponent<Bullet>().ID == gbmsg.Id);

        OtherPlayersBullet.Remove(bl);
        Destroy(bl);
    }

    private void HandleBullet(Game_Bullet gbmsg)
    {

        GameObject bl=OtherPlayersBullet.Find(b => b.GetComponent<Bullet>().ID == gbmsg.Id);
        bl.transform.position=new Vector2(gbmsg.x, gbmsg.y);

    }

    private void connectClientWithOpponnetListener(Game_SendAddress msg)
    {

            Debug.Log("got address " + msg.addresIp + " gotta create player object");
            GameObject.Find("_app").GetComponent<GameManger>().ServerIpAddr = msg.addresIp;
            gameObject.AddComponent<Client>();

        if (gameObject.GetComponent<DiscoveryListener>() != null)
        {
            gameObject.GetComponent<DiscoveryListener>().StopBroadcast();

            Destroy(gameObject.GetComponent<DiscoveryListener>());
        }

            //   ServerMessages.text = "CreatingClientForGame";
         
          
        
    }

    private void CreateandSendYourPO()
    {
        if (gameObject.GetComponent<Client>() != null)
        {
            gameObject.GetComponent<Client>().CreatePlayerObject();
        }
    }

    private void HandlePlayerDeath(Game_Death msg)
    {

        if(gameObject != null)
        {
            Destroy(playerObject);
        }
    }


}
