﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Client : MonoBehaviour //class that handels sending information to opponent about player movment/decisons
{
    //needed to init comunication
    private byte reliableChannel;

    private const int MAX_USER = 2;
    private  int PORT = 77777;
    private string serverIP = "";
    private const int BYTE_SIZE = 1024;
    private byte error;

    private int connectionId;
    private int hostId;

    private bool isStarted;
    GameObject playerObject;
    PlayerManager playerObjectManager;



    void Start()
    {
        if (GameObject.Find("_app").GetComponent<GameManger>().ishosting)
        {
            PORT++; //made to players have diffrent ports, useful for testing
        }

        DontDestroyOnLoad(this); //po przejściu między scenami obiekt nie będzi niszczony, inicjalizacja połączenia może się zacząć zanim gracz przejdzie z menu do sceny gry
        InitAndConnect(); //kilent po stworzniu próbuje się połączyć

        //wydarzenia które klient nasłuchuje i używa aby przesłać informacje 
        Server.DisconnectListeners += Reconnect;
        PlayerManager.OnPOSpawnedListeners += SendInitInfoaboutPO;
       // PlayerManager.OnDeathListeners += OnPlayerObjectDeath;
        Bullet.OnBirthListeners += OnBulletSpawned;
        Bullet.OnDeathListeners += OnBulletDied;
        Bullet.OnHitListeners += OnBulletHitPlayer;


    }


    public void CreatePlayerObject() 
    {
        if (playerObjectManager == null) //only create if player object don't exist
        {
            int selectedCharacter = GameObject.Find("_app").GetComponent<GameManger>().selectedCharacter; //ID are starting from 1
            if (GameObject.Find("_app").GetComponent<GameManger>(). ishosting)
            {
                playerObject = Instantiate(GameObject.Find("_app").GetComponent<GameManger>().playerPrefabs[selectedCharacter - 1], new Vector3(400, 300, 0), Quaternion.identity);
            }
            else
            {
                playerObject = Instantiate(GameObject.Find("_app").GetComponent<GameManger>().playerPrefabs[selectedCharacter - 1], new Vector3(200, 300, 0), Quaternion.identity);
            }

            playerObjectManager = playerObject.GetComponent<PlayerManager>();
            Character playerCH = GameObject.Find("_app").GetComponent<GameManger>().characters[selectedCharacter - 1];
            playerObject.name = "ClientPlayerObject";
        }
    }

    private void SendInitInfoaboutPO()
    {
        if (playerObject != null) //you never know, in case smth could go wrong with client creation
        {
           // Debug.Log("playerCreated gotta send it to other players");
            Game_Init initMsg = new Game_Init();
            initMsg.chosenchatacter = GameObject.Find("_app").GetComponent<GameManger>().selectedCharacter;
            initMsg.x = playerObject.transform.position.x;
            initMsg.y = playerObject.transform.position.y;
            sendToServer(initMsg); //sending message about just created player object to opponent
        }
    }

    private string LocalIPAddress() //returns ip addres of local machine
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }



    void InitAndConnect()
    {
        NetworkTransport.Init(); //inicjacaja modułu sieciowego

        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable); //tcp

        HostTopology topo = new HostTopology(cc, MAX_USER); 

        hostId = NetworkTransport.AddHost(topo, 0);

        string serveradd = GameObject.Find("_app").GetComponent<GameManger>().ServerIpAddr; //checking if class with global class have adress of server

        if (serveradd != "" )
        {
            Debug.Log("typed addres " + serveradd);
            serverIP = serveradd;
        }

        connectionId = NetworkTransport.Connect(hostId, serverIP, PORT, 0, out error);

    }

    //for brodcasting
    public void SendAddressToDiscoverdPlayer()
    {

        StartCoroutine(SendAddressAfterConnectionWithWait());
    }


    IEnumerator SendAddressAfterConnectionWithWait() //connectig with waiting
    {

        yield return new WaitForSecondsRealtime(2);
        if (connectionId != 0)
        {
          //  Debug.Log("connected to server gotta send IP");

            Game_SendAddress addMsg = new Game_SendAddress();
            addMsg.addresIp = LocalIPAddress();
            sendToServer(addMsg);
        }
    }

    public void SendAddresToServer() //sending a message that incloudes our ip address
    {
        
        if (connectionId != 0 && !GameObject.Find("_app").GetComponent<GameManger>().ishosting)
        {
            //Debug.Log("connected to server gotta send IP");

            Game_SendAddress addMsg = new Game_SendAddress();
            addMsg.addresIp = LocalIPAddress();
            sendToServer(addMsg);

            isStarted = true;

        }
        else
        {
            Debug.Log("connected error or not hosting");
        }
    }

    public void setIpAddress(string ip)
    {
        serverIP = ip;
    }

    public void Shutdown()
    {
        isStarted = false;
        NetworkTransport.Shutdown();
    }

    public void UpdateMessagePump() //client could get resp from server
    {
        if (!isStarted)
            return;

        //Debug.Log("works");
        int recHostId;
        int connectionId;
        int channelId;

        byte[] recBuffer = new byte[BYTE_SIZE];
        int datasize;

        NetworkEventType type = NetworkTransport.Receive(out hostId, out connectionId, out channelId, recBuffer, recBuffer.Length, out datasize, out error);

        switch (type)
        {
            case NetworkEventType.DataEvent:
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream(recBuffer);
                GameMessage msg = (GameMessage)formatter.Deserialize(memoryStream);
                OnData(connectionId, channelId, hostId, msg);
                // Debug.Log("data");
                break;

            case NetworkEventType.ConnectEvent:
                Debug.Log("Connected with server");
                break;

            case NetworkEventType.DisconnectEvent:
                Debug.Log("Disconnected from server");

                break;

            case NetworkEventType.BroadcastEvent:
                break;

            case NetworkEventType.Nothing:
                break;

            default:
                break;
        }
    }



    void sendToServer(GameMessage msg) //formating buffer and sending it
    {
        byte[] buffer = new byte[BYTE_SIZE];

        //Debug.Log(msg.operationCode);

        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream(buffer);
        formatter.Serialize(memoryStream, msg);

        

        NetworkTransport.Send(hostId, connectionId, reliableChannel, buffer, BYTE_SIZE, out error);
        Debug.Log("sending data to server");
    }



    private void OnData(int cnnId, int channelId, int hostid, GameMessage msg)
    {
        Debug.Log("recived " + msg.operationCode);
        switch (msg.operationCode)
        {
            case GameOP.None:
                break;

            case GameOP.Position:
                UpdatePosition((Game_Position)msg);
                break;
        }
    }

    private void UpdatePosition(Game_Position gpmsg)
    {
        Debug.Log("Position x" + gpmsg.x + " y: " + gpmsg.y);
    }

    void updatePositionOnServer()
    {
        if (playerObjectManager != null && playerObjectManager.joystick!=null)
        {
            if (playerObjectManager.joystick.isJoysticMoved() || playerObjectManager.grounded == false)
            {
                // Debug.Log("Client position x" + playerObject.transform.position.x + " y: " + playerObject.transform.position.x);
                Game_Position posMsg = new Game_Position();

                posMsg.x = playerObject.transform.position.x;
                posMsg.y = playerObject.transform.position.y;
                sendToServer(posMsg);
            }

        }
    }


    void Update()
    {

        updatePositionOnServer();
    }


    //callbacks

    void OnBulletDied(BulletInfo unitDeathInfo)
    {
       // Debug.Log("Bullet was destroyed by : " + unitDeathInfo.bulletInfo.name);

        Game_DestroyBullet bulletMsg = new Game_DestroyBullet();
        bulletMsg.Id = unitDeathInfo.bulletInfo.GetComponent<Bullet>().ID;
       // Debug.Log("MSG: " + bulletMsg.operationCode);
        sendToServer(bulletMsg);

    }

    void OnBulletHitPlayer(BulletInfo bulletInfo)
    {
      
        float actualHealth = playerObjectManager.Hp.currentValue - bulletInfo.damage;
        playerObjectManager.Hp.SetHealth(actualHealth);
        Game_PlayerLifeChanged bulletMsg = new Game_PlayerLifeChanged();
        bulletMsg.currentHealthValue = actualHealth;
        sendToServer(bulletMsg);
    }

    void OnBulletSpawned(BulletInfo bulletInfo)
    {

        Game_SpawnBullet bulletMsg = new Game_SpawnBullet();
        bulletMsg.Id = bulletInfo.bulletInfo.GetComponent<Bullet>().ID;
        bulletMsg.x = bulletInfo.bulletInfo.gameObject.transform.position.x;
        bulletMsg.y = bulletInfo.bulletInfo.gameObject.transform.position.y;
        bulletMsg.dirX = bulletInfo.bulletInfo.GetComponent<Bullet>().direction.x;
        bulletMsg.dirY = bulletInfo.bulletInfo.GetComponent<Bullet>().direction.y;
       // Debug.Log("MSG: " + bulletMsg.operationCode);
        sendToServer(bulletMsg);
    }



    void Reconnect()
    {
        NetworkTransport.Connect(hostId, serverIP, PORT, 0, out error);
    }

    public void SendReady()
    {
        Game_Ready rdy = new Game_Ready();
        sendToServer(rdy);
    }

}

