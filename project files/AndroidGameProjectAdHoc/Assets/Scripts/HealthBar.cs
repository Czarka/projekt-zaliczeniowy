﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour //obiekt odpowiedzialny za obsługę pasku życia
{
    private Image content;

    [SerializeField]
    private Text tekst;

    private float currentfill;

    private float lerpSpeed;

    public float MaxValue { get; set; }


    public float currentValue;


    public void SetHealth(float value)
    {
        if (value > MaxValue)
        {
            currentValue = MaxValue;
            // 
        }
        else if (currentValue < 0)
        {
            currentValue = 0;
        }
        else
        {
            currentValue = value;
        }

        currentfill = currentValue / MaxValue;
 
        tekst.text = currentValue + " / " + MaxValue;
    }

    void Start()
    {
        content = GetComponent<Image>();
        lerpSpeed = 1.0f; //prędkość animacji zmniejszania sie paska życia
    }

    void Update()
    {
        if (currentfill != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, currentfill, Time.deltaTime * lerpSpeed);
        }
    }

    public void init(float currentValue, float maxValue)
    {

        MaxValue = maxValue;
        SetHealth(currentValue);
        if (content != null)
        {
            content.fillAmount = 0.1f;
        }
    }
}
