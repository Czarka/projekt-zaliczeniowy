﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManger : MonoBehaviour //this class store global data, states
{

    public string ServerIpAddr="";
    public bool ishosting = false;

    public Character[] characters;

    public GameObject[] playerPrefabs;

    public GameObject[] OpponentPrefabs;

    public int selectedCharacter;

    public int selectedOpponentCharacter;


    public void setServerIpAddr(string ipaddr)
    {
        ServerIpAddr = ipaddr;
    }

    public string getServerIpAddr()
    {
        return ServerIpAddr;
    }

    void Awake()
    {
        SceneManager.LoadScene("Menu");
    }

}
