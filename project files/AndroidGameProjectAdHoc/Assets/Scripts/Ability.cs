﻿using UnityEngine;
using System.Collections;

public abstract class Ability : ScriptableObject
    //an interface for Ablities that are ScriptableObjects
{
    public string aName = "New Ability";
    public Sprite aSprite;
    public float aBaseCoolDown = 1f;

    public abstract void Initialize(GameObject obj);
    public abstract void TriggerAbility();
}
