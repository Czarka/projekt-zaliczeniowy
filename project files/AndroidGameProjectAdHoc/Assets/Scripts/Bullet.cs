﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Vector2 velocity;
    public float speed;
    public Vector2 direction;
    public int ID;
    Rigidbody2D rb2d;

    public int damage=5;

    public delegate void OnDeathCallbackDelegate(BulletInfo bulletDeathInfo);
    static public event OnDeathCallbackDelegate OnDeathListeners;

    public delegate void OnBirthCallbackDelegate(BulletInfo bulletDeathInfo);
    static public event OnBirthCallbackDelegate OnBirthListeners;

    public delegate void OnHitCallbackDelegate(BulletInfo bulletInfo);
    static public event OnBirthCallbackDelegate OnHitListeners;

    //events asociated with bullets

    public void Init(Vector2 dir, int Id)
    {
        ID = Id;

        direction = dir;

        velocity = direction * speed;


        if (OnBirthListeners != null)
        {
            BulletInfo udi = new BulletInfo();
            udi.bulletInfo = gameObject;
            OnBirthListeners(udi);
        }

    }
    public void InitOnServer(Vector2 direction, int Id)
    { //function that is used on listner module that inits bullets for opponent
        
        ID = Id;
        velocity = direction * speed;
        gameObject.tag = "enemybullet";
    }

        void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        rb2d.velocity = velocity;
    }

    void DestroyBulletAndNotifyLocal()
    {
        if (gameObject.tag != "enemybullet")
        {
            if (OnDeathListeners != null)
            {
                BulletInfo udi = new BulletInfo();
                udi.bulletInfo = gameObject;
                OnDeathListeners(udi);
            }
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible() // function triggerd when bullet is out of camera scope
    {
        DestroyBulletAndNotifyLocal();
    }

    void OnTriggerEnter2D(Collider2D colide) //triggers when bullet collides, this function checks what is hapening
    {

        switch (colide.tag)
        {
            case "terrain":
                DestroyBulletAndNotifyLocal();
                break;

            case "enemyplayer":
              
                if (gameObject.tag == "playerbullet")
                {
                    DestroyBulletAndNotifyLocal();
                }
                    break;

            case "enemybullet":
                DestroyBulletAndNotifyLocal();
                break;

            case "playerbullet":
                if (gameObject.tag == "enemybullet")
                {
                    if (OnDeathListeners != null)
                    {
                       
                        BulletInfo udi = new BulletInfo();
                        udi.bulletInfo = gameObject;
                        OnDeathListeners(udi);
                    }

                    Destroy(gameObject);
                  }
                break;

            case "localplayer":

                if (gameObject.tag == "enemybullet")
                {

                    if (OnHitListeners != null)
                    {
                        BulletInfo ui = new BulletInfo();
                        ui.bulletInfo = gameObject;
                        ui.damage = damage;
                        OnHitListeners(ui);
                    }


                    if (OnDeathListeners != null)
                    {
                        BulletInfo udi = new BulletInfo();
                        udi.bulletInfo = gameObject;
                        OnDeathListeners(udi);
                    }

                    Destroy(gameObject);
                }
                break;
        }

       
    }
}
