﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour
{

    public GameObject networkPanel;
    public GameObject CharacterSelectPanel;

    GameObject networkConfigurationObject;

    GameObject PlayerController;
    bool listening = false;

    void Start()
    {
        //DontDestroyOnLoad(this);
        networkConfigurationObject = new GameObject();
        CharacterSelectPanel.SetActive(false);
    }

    void Update()
    {
        
    }

    public void StartGame(int characterChoice)
    {

        SceneManager.LoadScene("SampleScene");

        GameObject.Find("_app").GetComponent<GameManger>().selectedCharacter = characterChoice;

        if(!listening && GameObject.Find("_app").GetComponent<GameManger>().ServerIpAddr!="" )
        {
            PlayerController.GetComponent<Client>().SendAddresToServer(); //if connected send addres your address to listenr who adress you typed
        }
      
    }


    public void instantiateClient()
    {
      

        string ipaddr = GameObject.Find("InputIpAddress").GetComponent<TMP_InputField>().text;
        if (ipaddr != null)
        {
            GameObject.Find("_app").GetComponent<GameManger>().ServerIpAddr = ipaddr;
            PlayerController = Instantiate(networkConfigurationObject, transform.position, transform.rotation);
            PlayerController.AddComponent<Server>();
            PlayerController.name = "PlayerControler";
            Debug.Log("typed addres " + ipaddr);
            if (ipaddr == "")
            {
                PlayerController.AddComponent<DiscoverGame>();
            }
            else
            {
                PlayerController.AddComponent<Client>();
            }
          


            networkPanel.SetActive(false);
            CharacterSelectPanel.SetActive(true);

           
           
           
        }
    }

    public void instantiateListener()
    {
      
        GameObject.Find("_app").GetComponent<GameManger>().ishosting = true;
        listening = true;
        networkPanel.SetActive(false);
        CharacterSelectPanel.SetActive(true);

        PlayerController = Instantiate(networkConfigurationObject, transform.position, transform.rotation);
        PlayerController.AddComponent<Server>();
        PlayerController.AddComponent<DiscoveryListener>();
        PlayerController.name = "PlayerControler";

    }
}
