﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsManager : MonoBehaviour //class that helps configure physics system provided by Unity Engine
{   //used to program player physics
    public float minGroundNormalY = 6f;
    public float gravityModifier = 30f;

    protected Rigidbody2D rb2d;
    protected Vector2 velocity;
    protected Vector2 targetVelocity;
    public bool grounded;

    protected Vector2 GroundNormal;

    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    protected const float minMoveDistance = 0.01f;
    protected const float shellRadius = 2f;
    // Start is called before the first frame update
    void Start()
    {
        //configure contact filter for collsion
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
      
    }

    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    void FixedUpdate()
    {
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;  //gravity
        velocity.x = targetVelocity.x;

        grounded = false;

        Vector2 moveOnGround = new Vector2(GroundNormal.y, -GroundNormal.x);

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 move = moveOnGround * deltaPosition.x;

        GravityMovment(move, false);

        move = Vector2.up * deltaPosition.y;

        GravityMovment(move, true);
    }

    void GravityMovment(Vector2 move, bool yMovment)
    {
        float distance = move.magnitude;

        if (distance > minMoveDistance)
        {
            int count = rb2d.Cast(move, contactFilter, hitBuffer, shellRadius);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                if(currentNormal.y>minGroundNormalY)
                {
                    grounded = true;
                    if (yMovment)
                    {
                        GroundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal); //różnica między predkością i normalna i można dzieki temu można sprawdzić jak się ruszyć po kolizji

                if(projection < 0)
                {
                    velocity = velocity - projection * currentNormal;
                }


                //aby unknać zagnieżdzenia się w kolejnych coliderach

                float modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }

        }

        rb2d.position = rb2d.position + move.normalized * distance;
    }
}
