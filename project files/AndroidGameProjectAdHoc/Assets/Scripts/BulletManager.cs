﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour //this class manages bullet abilities having in mind paramaters taha are discribed in ScriptableObjects
{

    public GameObject bulletPrefab; //wzorcowy obiekt kuli 
    public Vector2 bulletDirection;


    private int bulletIdCounter=0;

    public delegate void OnUnitSpawnedDelegate(Bullet bullet);
    public event OnUnitSpawnedDelegate OnUnitSpawnedListeners;

    private int amount_of_bullets=1;
    private float time_interval=0;
    private float currCountdownValue;
    private int space_between_bullets = 0;


    public void Init(int amountOfBullets, float timeInterval,int spaceBetweenBullets)
    {
        amount_of_bullets = amountOfBullets;
        time_interval = timeInterval;
        space_between_bullets = spaceBetweenBullets;
    }

    public void Fire()
    {
        if (time_interval != 0f)
        {
            StartCoroutine(WaitForSpawnBullet()); //for bullets with time intervals
        }
        else
        {
            for (int i = 0; i < amount_of_bullets; i++)
            {
                if (space_between_bullets != 0)
                {
                    bulletDirection.y += 0.1f;
                    if (i > (amount_of_bullets / 2))
                    {
                        bulletDirection.x -= i * space_between_bullets / 20;
                        bulletDirection.y -= i * space_between_bullets / 20;
                    }
                    else
                    {
                        bulletDirection.x += i/2 * space_between_bullets / 20;
                        bulletDirection.y += i/2 * space_between_bullets / 20;
                    }

                }

                GameObject go = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                go.GetComponent<Bullet>().Init(bulletDirection, bulletIdCounter);
                bulletIdCounter++; //kula potrzebuje Id aby możliwe było zydentfikowanie jaką kule trzeba później usunąc z listy przeciwnika


                if (OnUnitSpawnedListeners != null)
                {
                    OnUnitSpawnedListeners(go.GetComponent<Bullet>());
                }
            }
        }
       
    }

    IEnumerator WaitForSpawnBullet()
    {
        //print(Time.time);
        for (int i = 0; i < amount_of_bullets; i++)
        {
           
            GameObject go = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            go.GetComponent<Bullet>().Init(bulletDirection, bulletIdCounter);
            bulletIdCounter++;


            if (OnUnitSpawnedListeners != null)
            {
                OnUnitSpawnedListeners(go.GetComponent<Bullet>());
            }
            
            yield return new WaitForSecondsRealtime(time_interval);
        }
    }

}
