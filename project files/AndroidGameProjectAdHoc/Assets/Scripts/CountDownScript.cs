﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownScript : MonoBehaviour //for buttons to show countdown for skills
{

    public float countdownTime = 5;//deafualt time

    private float nextSkillUseTime = 0;

    [SerializeField]
    private Button button;

    [SerializeField]
    private Image countdownIlustrator;



    // Start is called before the first frame update
    void Start()
    {
        button = gameObject.GetComponent<Button>();
        button.GetComponent<Button>().onClick.AddListener(skillUsed);
        countdownIlustrator.fillAmount = 0;
    }


    void skillUsed()
    {
        nextSkillUseTime = Time.time + countdownTime;
        countdownIlustrator.fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time> nextSkillUseTime)
        {
            if (button.interactable == false)
            {
                button.interactable = true;
            }
           
        }
        else
        {
            if (button.interactable == true)
            {
                button.interactable =  false;
            }
            if (countdownIlustrator.fillAmount != 0)
                countdownIlustrator.fillAmount -= 1.0f / countdownTime * Time.deltaTime;
        }
    }
}
