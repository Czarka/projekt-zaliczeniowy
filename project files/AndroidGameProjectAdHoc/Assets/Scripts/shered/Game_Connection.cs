﻿[System.Serializable] 
public class Game_SendAddress : GameMessage//message used for sending plater adress
{


    public string addresIp { get; set; }


    public Game_SendAddress()
    {

        operationCode = GameOP.SendingAddres;

    }
}


[System.Serializable]
public class Game_Ready : GameMessage //message used for notify that player has connected (besides Connect event)
{


    public Game_Ready()
    {

        operationCode = GameOP.Ready;

    }
}



