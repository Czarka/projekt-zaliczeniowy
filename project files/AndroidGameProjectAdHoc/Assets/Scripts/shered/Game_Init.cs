﻿[System.Serializable]
public class Game_Init : GameMessage //message used for init a player object
{

    public float x { get; set; }
    public float y { get; set; }
    public int chosenchatacter { get; set; }

    public Game_Init()
    {
        
        operationCode = GameOP.Init;
       
    }
}
