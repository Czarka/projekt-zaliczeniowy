﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameOP //Operation codes for game msges
{
    public const int None = 0;
    public const int Position = 1;
    public const int Init = 2;

    public const int BulletInformation = 3;
    public const int DestroyBullet = 4;
    public const int BulledSpawned = 5;

    public const int Health = 6;


    public const int SendingAddres = 7;
    public const int Ready = 8;

    public const int Death = 9;

}

[System.Serializable]
public abstract class GameMessage //interface for GameMessages
{

    public byte operationCode { get; set; }

    public GameMessage()
    {
    }
}
