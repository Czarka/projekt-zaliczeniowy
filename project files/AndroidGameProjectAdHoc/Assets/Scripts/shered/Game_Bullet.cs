﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Game_Bullet : GameMessage //message used for updaing bullet/ not used right now, player device has to simulate postion after geting a msg about bullet creation
{
    public float x { get; set; }
    public float y { get; set; }

    public int Id { get; set; }

    //public int player;

    public Game_Bullet()
    {
        operationCode = GameOP.BulletInformation;
    }

}

[System.Serializable]
public class Game_DestroyBullet : GameMessage //message used for assuring that bullet was destroyed
{

    public int Id { get; set; }

    //public int player;

    public Game_DestroyBullet()
    {
        operationCode = GameOP.DestroyBullet;
    }

}


[System.Serializable]
public class Game_SpawnBullet : GameMessage //message used for signalazing that player has shoot bullet
{
    public float x { get; set; }
    public float y { get; set; }

    public float dirX { get; set; }
    public float dirY { get; set; }

    public int Id { get; set; }


    public Game_SpawnBullet()
    {
        operationCode = GameOP.BulledSpawned;
    }

}


