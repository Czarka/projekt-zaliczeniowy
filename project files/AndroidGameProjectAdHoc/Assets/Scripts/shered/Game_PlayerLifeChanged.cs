﻿[System.Serializable]
public class Game_PlayerLifeChanged: GameMessage  //message used for updaing player life/lifebars
{

    public float currentHealthValue { get; set; }
    //PlayerUnit playerdata;

    public Game_PlayerLifeChanged()
    {

        operationCode = GameOP.Health;

    }
}

