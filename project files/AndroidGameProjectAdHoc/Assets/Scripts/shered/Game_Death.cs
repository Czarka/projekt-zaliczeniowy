﻿[System.Serializable]
public class Game_Death : GameMessage //message used for assuring players death on oponnent device
{

    public Game_Death()
    {

        operationCode = GameOP.Death;

    }
}
