﻿[System.Serializable]
public class Game_Position : GameMessage  //message used for updaing player position
{
 
    public float x { get; set; }
    public float y { get; set; }

    public Game_Position(float positionx, float positiony)
    {
        operationCode = GameOP.Position;
        x = positionx;
        y = positiony;
    }

    public Game_Position()
    {
        //operationCode = GameOP.Position;
        operationCode = GameOP.Position;
    }
}

