﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleSceneManager : MonoBehaviour
{
    [SerializeField]
    GameObject EndScreen; //an window that is displayed after one of players die

    [SerializeField]
    GameObject DisconnectScreen;// an window that is displayed after one of players disconnected and didn't reconnected


    void Start()
    {
        EndScreen.SetActive(false);
        DisconnectScreen.SetActive(false);

        PlayerManager.OnDeathListeners += OnGameEnded;
        Server.OpponentLeftListeners += OnPlayerDisconnected;
    }


    void OnPlayerDisconnected()
    {
        if (DisconnectScreen == null)
        {
            DisconnectScreen = GameObject.Find("PlayerDisconnected");
        }
        DisconnectScreen.SetActive(true);
    }

    void OnGameEnded()
    {
        if (EndScreen == null)
        {
            EndScreen = GameObject.Find("EndScreen");
        }
        Destroy(GameObject.Find("ClientPlayerObject"));  //can try to destroy non existent object
        Destroy(GameObject.Find("OpponentPlayerObject"));
        EndScreen.SetActive(true);
    }



    public void ExitGame()
    {
        Destroy(GameObject.Find("ClientPlayerObject"));
        Destroy(GameObject.Find("OpponentPlayerObject"));
        Server serverobject = GameObject.Find("PlayerControler").GetComponent<Server>();
        serverobject.Shutdown();
        Destroy(GameObject.Find("PlayerControler"));
        SceneManager.LoadScene("Menu");
        Debug.Log("exxxxxiting");
        EndScreen.SetActive(false);
        Destroy(EndScreen);
        PlayerManager.OnDeathListeners -= OnGameEnded;
    }

    public void Rematch()
    {
   
        EndScreen.SetActive(false);

        Client client = GameObject.Find("PlayerControler").GetComponent<Client>();
        client.CreatePlayerObject();
        Debug.Log("reeeeeeeeeeeeeematch");
      
    }
}
